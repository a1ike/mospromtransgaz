$(document).ready(function () {

  $('.phone').inputmask('+7(999)999-99-99');

  $('.m-home-reviews__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }]
  });

  $('.m-header__toggle').on('click', function (e) {
    e.preventDefault();
    $('.m-info').slideToggle('fast');
    $('.m-header__nav').slideToggle('fast');
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.m-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.m-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.m-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

});
